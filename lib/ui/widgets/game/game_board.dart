import 'package:flutter/material.dart';

import 'package:reversi/game/game_engine.dart';

class GameBoardWidget extends StatelessWidget {
  const GameBoardWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: const GameEngine(),
    );
  }
}
