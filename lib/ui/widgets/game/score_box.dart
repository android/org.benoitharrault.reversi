import 'package:flutter/material.dart';

import 'package:reversi/config/styling.dart';
import 'package:reversi/models/activity/game_board.dart';
import 'package:reversi/models/activity/game_model.dart';

class ScoreBox extends StatelessWidget {
  const ScoreBox({
    super.key,
    required this.player,
    required this.model,
  });

  final PieceType player;
  final GameModel model;

  @override
  Widget build(BuildContext context) {
    var assetImageCode = player == PieceType.black ? 'black' : 'white';
    String assetImageName = 'assets/skins/${model.skin}_tile_$assetImageCode.png';

    var scoreText = player == PieceType.black ? '${model.blackScore}' : '${model.whiteScore}';

    return Container(
      padding: const EdgeInsets.symmetric(
        vertical: 3.0,
        horizontal: 25.0,
      ),
      decoration: (model.player == player)
          ? Styling.activePlayerIndicator
          : Styling.inactivePlayerIndicator,
      child: Row(
        children: <Widget>[
          SizedBox(
            width: 25.0,
            height: 25.0,
            child: Image(
              image: AssetImage(assetImageName),
              fit: BoxFit.fill,
            ),
          ),
          const SizedBox(
            width: 10.0,
          ),
          Text(
            scoreText,
            textAlign: TextAlign.center,
            style: const TextStyle(
              fontSize: 35.0,
              color: Color(0xff000000),
            ),
          ),
        ],
      ),
    );
  }
}
