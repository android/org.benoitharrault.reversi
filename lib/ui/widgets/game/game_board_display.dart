import 'package:flutter/material.dart';

import 'package:reversi/config/styling.dart';
import 'package:reversi/models/activity/game_board.dart';
import 'package:reversi/models/activity/game_model.dart';

class GameBoardDisplay extends StatelessWidget {
  const GameBoardDisplay({
    super.key,
    required this.context,
    required this.model,
    required this.callback,
  });

  final BuildContext context;
  final GameModel model;
  final Function callback;

  @override
  Widget build(BuildContext context) {
    final rows = <TableRow>[];

    for (int y = 0; y < GameBoard.height; y++) {
      final cells = <Column>[];

      for (int x = 0; x < GameBoard.width; x++) {
        PieceType pieceType = model.board.getPieceAtLocation(x, y);
        String? assetImageCode = Styling.assetImageCodes[pieceType];
        String assetImageName =
            'assets/skins/${model.skin}_tile_${assetImageCode ?? 'empty'}.png';

        Column cell = Column(
          children: [
            Container(
              decoration: Styling.boardCellDecoration,
              child: SizedBox(
                child: GestureDetector(
                  child: AnimatedSwitcher(
                    duration: const Duration(milliseconds: 250),
                    transitionBuilder: (Widget child, Animation<double> animation) {
                      return ScaleTransition(
                        scale: animation,
                        child: child,
                      );
                    },
                    child: Image(
                      image: AssetImage(assetImageName),
                      fit: BoxFit.fill,
                      key: ValueKey<int>(
                        pieceType == PieceType.empty
                            ? 0
                            : (pieceType == PieceType.black ? 1 : 2),
                      ),
                    ),
                  ),
                  onTap: () {
                    callback(model, x, y);
                  },
                ),
              ),
            )
          ],
        );

        cells.add(cell);
      }

      rows.add(TableRow(
        children: cells,
      ));
    }

    return Table(
      defaultColumnWidth: const IntrinsicColumnWidth(),
      children: rows,
    );
  }
}
