import 'package:flutter/material.dart';

import 'package:reversi/ui/widgets/game/game_board.dart';

class PageGame extends StatelessWidget {
  const PageGame({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: AlignmentDirectional.topCenter,
      padding: const EdgeInsets.all(4),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const GameBoardWidget(),
          const Expanded(child: SizedBox.shrink()),
        ],
      ),
    );
  }
}
