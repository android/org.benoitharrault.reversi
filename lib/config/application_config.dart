import 'package:flutter/material.dart';
import 'package:flutter_custom_toolbox/flutter_toolbox.dart';

import 'package:reversi/cubit/activity/activity_cubit.dart';

import 'package:reversi/ui/pages/game.dart';

class ApplicationConfig {
  // activity parameter: skin
  static const String parameterCodeSkin = 'global.skin';
  static const String skinValueDefault = 'default';

  // activity parameter: game mode
  static const String parameterCodeGameMode = 'gameMode';
  static const String gameModeValueMedium = 'human-vs-cpu';

  // activity parameter: difficulty level
  static const String parameterCodeDifficultyLevel = 'difficultyLevel';
  static const String difficultyLevelMedium = 'medium';


  // activity pages
  static const int activityPageIndexHome = 0;
  static const int activityPageIndexGame = 1;

  static final ApplicationConfigDefinition config = ApplicationConfigDefinition(
    appTitle: 'Reversi',
    activitySettings: [
      // skin
      ApplicationSettingsParameter(
        code: parameterCodeSkin,
        values: [
          ApplicationSettingsParameterItemValue(
            value: skinValueDefault,
            isDefault: true,
          ),
        ],
      ),

      // game mode
      ApplicationSettingsParameter(
        code: parameterCodeGameMode,
        values: [
          ApplicationSettingsParameterItemValue(
            value: gameModeValueMedium,
            isDefault: true,
          ),
        ],
      ),

      // difficulty level
      ApplicationSettingsParameter(
        code: parameterCodeDifficultyLevel,
        values: [
          ApplicationSettingsParameterItemValue(
            value: difficultyLevelMedium,
            isDefault: true,
          ),
        ],
      ),
    ],
    startNewActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).startNewActivity(context);
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexGame);
    },
    quitCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).quitActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexHome);
    },
    deleteCurrentActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).deleteSavedActivity();
    },
    resumeActivity: (BuildContext context) {
      BlocProvider.of<ActivityCubit>(context).resumeSavedActivity();
      BlocProvider.of<NavCubitPage>(context)
          .updateIndex(ApplicationConfig.activityPageIndexGame);
    },
    navigation: ApplicationNavigation(
      screenActivity: ScreenItem(
        code: 'screen_activity',
        icon: Icon(UniconsLine.home),
        screen: ({required ApplicationConfigDefinition appConfig}) =>
            ScreenActivity(appConfig: appConfig),
      ),
      screenSettings: ScreenItem(
        code: 'screen_settings',
        icon: Icon(UniconsLine.setting),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenSettings(),
      ),
      screenAbout: ScreenItem(
        code: 'screen_about',
        icon: Icon(UniconsLine.info_circle),
        screen: ({required ApplicationConfigDefinition appConfig}) => ScreenAbout(),
      ),
      activityPages: {
        activityPageIndexHome: ActivityPageItem(
          code: 'page_home',
          icon: Icon(UniconsLine.home),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageParameters(
                  appConfig: appConfig,
                  canBeResumed: activityState.currentActivity.canBeResumed,
                );
              },
            );
          },
        ),
        activityPageIndexGame: ActivityPageItem(
          code: 'page_game',
          icon: Icon(UniconsLine.star),
          builder: ({required ApplicationConfigDefinition appConfig}) {
            return BlocBuilder<ActivityCubit, ActivityState>(
              builder: (BuildContext context, ActivityState activityState) {
                return PageGame();
              },
            );
          },
        ),
      },
    ),
  );
}
