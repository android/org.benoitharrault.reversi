import 'package:flutter/widgets.dart';

import 'package:reversi/models/activity/game_board.dart';

abstract class Styling {
  // **** GRADIENTS AND COLORS ****

  static const Map<PieceType, String> assetImageCodes = {
    PieceType.black: 'black',
    PieceType.white: 'white',
    PieceType.empty: 'empty',
  };

  static const BorderSide boardCellBorderSide = BorderSide(
    color: Color(0xff108010),
    width: 1.0,
  );

  static const BoxDecoration boardCellDecoration = BoxDecoration(
    color: Color(0xff50bb50),
    border: Border(
      bottom: boardCellBorderSide,
      top: boardCellBorderSide,
      left: boardCellBorderSide,
      right: boardCellBorderSide,
    ),
  );

  static const BoxDecoration mainWidgetDecoration = BoxDecoration(color: Color(0xffffffff));

  static const thinkingColor = Color(0xff2196f3);

  // **** ANIMATIONS ****

  static const Duration thinkingFadeDuration = Duration(milliseconds: 500);

  static const pieceFlipDuration = Duration(milliseconds: 300);

  // **** SIZES ****

  static const thinkingSize = 10.0;

  // **** BOXES ****

  static const BorderSide activePlayerIndicatorBorder = BorderSide(
    color: Color(0xff2196f3),
    width: 10.0,
  );

  static const BorderSide inactivePlayerIndicatorBorder = BorderSide(
    color: Color(0x00000000),
    width: 10.0,
  );

  static const activePlayerIndicator = BoxDecoration(
    borderRadius: BorderRadius.all(Radius.circular(10.0)),
    border: Border(
      bottom: activePlayerIndicatorBorder,
      top: activePlayerIndicatorBorder,
      left: activePlayerIndicatorBorder,
      right: activePlayerIndicatorBorder,
    ),
  );

  static const inactivePlayerIndicator = BoxDecoration(
    borderRadius: BorderRadius.all(Radius.circular(10.0)),
    border: Border(
      bottom: inactivePlayerIndicatorBorder,
      top: inactivePlayerIndicatorBorder,
      left: inactivePlayerIndicatorBorder,
      right: inactivePlayerIndicatorBorder,
    ),
  );
}
